package com.example.region.feign;

import com.example.region.payload.CountryDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "COUNTRY")
public interface CountryFeign {

    @GetMapping("/country/{id}")
    CountryDto getById(@PathVariable Integer id);

}
