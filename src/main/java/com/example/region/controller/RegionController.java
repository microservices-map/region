package com.example.region.controller;

import com.example.region.entity.Region;
import com.example.region.feign.CountryFeign;
import com.example.region.payload.CountryDto;
import com.example.region.payload.RegionDto;
import com.example.region.repository.RegionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/region")
public class RegionController {

    @Autowired
    private RegionRepository regionRepository;
    @Autowired
    private CountryFeign countryFeign;

    @GetMapping
    public List<Region> get(@PathVariable Integer countryId) {
        List<Region> regionList = regionRepository.findAll();
        return regionList;
    }

    @PostMapping
    public String add(@RequestBody RegionDto regionDto) {
        CountryDto countryDto = countryFeign.getById(regionDto.getCountryId());
        if (countryDto != null) {
            Region region = new Region(
                    null,
                    regionDto.getName(),
                    regionDto.getCountryId()
            );
            regionRepository.save(region);
            return "ok";
        }
        return "error";
    }

}
